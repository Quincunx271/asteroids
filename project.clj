(defproject asteroids "0.1.0"
  :description "An implementation of the classic arcade game Asteroids"
  :url "https://bitbucket.org/Quincunx271/asteroids"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]])
